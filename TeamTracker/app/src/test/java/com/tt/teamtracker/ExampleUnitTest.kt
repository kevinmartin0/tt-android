package com.tt.teamtracker

import com.tt.teamtracker.utils.lengthBetween
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun test_between_4_between_3_and_35() {
        val testString = "teststring"
        assertEquals(true, lengthBetween(testString, 3, 35))
    }
}
