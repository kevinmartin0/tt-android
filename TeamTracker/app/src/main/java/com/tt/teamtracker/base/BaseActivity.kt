package com.tt.teamtracker.base

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.tt.teamtracker.ui.register.athlete.RegisterAthleteActivity

abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel>() : AppCompatActivity() {
	protected lateinit var binding: T
	protected lateinit var viewModel: V

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding = DataBindingUtil.setContentView(this, getLayoutId())
		viewModel = ViewModelProviders.of(this).get(getVMClass())
		initViewModelBinding()
		title = getActivityTitle()

	}

	@LayoutRes
	abstract fun getLayoutId(): Int

	abstract fun initViewModelBinding()

	abstract fun getVMClass(): Class<V>

	abstract fun getActivityTitle(): String

	protected fun addFragment(fragment: Fragment, addToBackStack: Boolean, tag: String, container: Int) {
		val manager = supportFragmentManager
		val ft = manager.beginTransaction()

		if (addToBackStack) {
			ft.addToBackStack(tag)
		}
		ft.replace(container, fragment, tag)
		ft.commitAllowingStateLoss()
	}

	protected fun startNewActivity(activity: Class<*>, addToBackStack: Boolean) {
		val intent = Intent(this, activity)
		if (!addToBackStack) intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
		startActivity(intent)
	}
}