package com.tt.teamtracker.model

import com.google.gson.annotations.SerializedName

data class Invite (
		@SerializedName("id")
		var id: Int,

		@SerializedName("user_id")
        var userId: Int,

		@SerializedName("team_id")
		var teamId: Int,

		@SerializedName("referral_code")
		var referralCode: Int,

		@SerializedName("email")
		var email: String,

		@SerializedName("role_type")
		var roleType: String,

		@SerializedName("expires_at")
		var expiresAt: String,

		@SerializedName("active")
		var active: Int
)