package com.tt.teamtracker.ui.login

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.widget.Toast
import com.tt.teamtracker.R
import com.tt.teamtracker.base.App
import com.tt.teamtracker.base.BaseActivity
import com.tt.teamtracker.databinding.ActivityLoginBinding
import com.tt.teamtracker.model.User
import com.tt.teamtracker.ui.dashboard.DashboardActivity
import com.tt.teamtracker.ui.register.select.SelectRegistrationTypeActivity
import com.tt.teamtracker.utils.SharedPreferencesManager
import javax.inject.Inject

class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		observeNavigateDashboard()
		observeNavigateRegister()
		observeLoading()
	}

	private fun observeNavigateDashboard() {
		viewModel.navigateDashboardEvent.observe(this, Observer {
			startNewActivity(DashboardActivity::class.java, false)
		})
	}

	private fun observeNavigateRegister() {
		viewModel.navigateRegisterEvent.observe(this, Observer {
			startNewActivity(SelectRegistrationTypeActivity::class.java, false)
		})
	}

	private fun observeLoading() {
		viewModel.loadingEvent.observe(this, Observer { visibility -> binding.progressBar.visibility = visibility!! })
	}

	override fun getLayoutId(): Int {
		return R.layout.activity_login
	}

	override fun initViewModelBinding() {
		binding.viewModel = viewModel
	}

	override fun getVMClass(): Class<LoginViewModel> {
		return LoginViewModel::class.java
	}

	override fun getActivityTitle(): String {
		return "Login"
	}

}