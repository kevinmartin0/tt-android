package com.tt.teamtracker.repository

import com.tt.teamtracker.base.BaseRepository
import com.tt.teamtracker.network.response.ApiSuccessResponse
import com.tt.teamtracker.network.response.GetActivitiesResponse
import io.reactivex.Observable

class ActivityRespository : BaseRepository() {

	fun getUserActivities(token: String): Observable<ApiSuccessResponse<GetActivitiesResponse>>{
		return teamTrackerApi.getActivities(token)
	}

}