package com.tt.teamtracker.utils.errorEvents

import android.support.annotation.StringRes

interface ResourceError {
    @StringRes
    fun getErrorResource(): Int
}