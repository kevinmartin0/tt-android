package com.tt.teamtracker.ui.dashboard

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import com.tt.teamtracker.R
import com.tt.teamtracker.base.BaseActivity
import com.tt.teamtracker.databinding.ActivityDashboardBinding
import com.tt.teamtracker.ui.activities.ActivitiesFragment
import com.tt.teamtracker.ui.login.LoginActivity

class DashboardActivity : BaseActivity<ActivityDashboardBinding, DashboardViewModel>() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		initBottomNavigation()
		observeLoading()
		observeLogout()
	}

	private fun observeLogout() {
		viewModel.logoutEvent.observe(this, Observer { startNewActivity(LoginActivity::class.java, false) })
	}

	private fun initBottomNavigation() {
		binding.dashboardBottomNavbar.setOnNavigationItemSelectedListener { item: MenuItem ->
			when (item.itemId) {
				R.id.navigation_activities -> {
					Toast.makeText(this, "Activities", Toast.LENGTH_LONG).show()
					addFragment(ActivitiesFragment(), false, "activities", getFragmentContainer())
					true
				}

				R.id.navigation_events -> {
					Toast.makeText(this, "Events", Toast.LENGTH_LONG).show()
					true
				}

				R.id.navigation_account -> {
					Toast.makeText(this, "Account", Toast.LENGTH_LONG).show()
					viewModel.logout()
					true
				}
				else -> {
					false
				}
			}
		}
		binding.dashboardBottomNavbar.selectedItemId = R.id.navigation_activities
	}

	private fun observeLoading() {
		viewModel.loadingEvent.observe(this, Observer { visibility -> binding.progressBar.visibility = visibility!! })
	}

	private fun getFragmentContainer(): Int {
		return R.id.container
	}

	override fun getLayoutId(): Int {
		return R.layout.activity_dashboard
	}

	override fun initViewModelBinding() {
		binding.viewModel = viewModel
	}

	override fun getVMClass(): Class<DashboardViewModel> {
		return DashboardViewModel::class.java
	}

	override fun getActivityTitle(): String {
		return "Dashboard"
	}

}