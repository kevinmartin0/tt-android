package com.tt.teamtracker.ui.register.athlete

import android.arch.lifecycle.MutableLiveData
import com.tt.teamtracker.base.BaseViewModel
import com.tt.teamtracker.repository.InviteRepository
import com.tt.teamtracker.repository.UserRepository
import com.tt.teamtracker.network.request.RegisterAthleteRequest

import com.tt.teamtracker.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import javax.inject.Inject
import com.tt.teamtracker.network.request.ValidateInviteRequest
import com.tt.teamtracker.utils.errorEvents.ErrorWrapper


class RegisterAthleteViewModel : BaseViewModel() {


	@Inject
	lateinit var userRepository: UserRepository

	@Inject
	lateinit var inviteRepository: InviteRepository

	private var subscription: Disposable? = null

	val curStep = MutableLiveData<Int?>()

	val email = MutableLiveData<String>()
	val inviteCode = MutableLiveData<String>()
	val firstName = MutableLiveData<String>()
	val lastName = MutableLiveData<String>()
	val password = MutableLiveData<String>()
	val passwordConfirmation = MutableLiveData<String>()

	val emailError = MutableLiveData<ErrorWrapper?>()
	val inviteCodeError = MutableLiveData<ErrorWrapper?>()
	val firstNameError = MutableLiveData<ErrorWrapper?>()
	val lastNameError = MutableLiveData<ErrorWrapper?>()
	val passwordError = MutableLiveData<ErrorWrapper?>()
	val passwordConfirmationError = MutableLiveData<ErrorWrapper?>()

	val navigateLoginEvent = SingleLiveEvent<Any>()

	init {
		curStep.value = 1
	}

	fun navigateLogin() {
		this.navigateLoginEvent.call()
	}

	fun next() {
		if (!isLoading()) {
			when (curStep.value) {
				1 -> validateStep1()
				2 -> validateStep2()
			}
		}
	}

	fun back() {
		if (!isLoading()) {
			when (curStep.value) {
				2 -> decrementStep()
			}
		}
	}

	private fun validateStep1() {
		emailError.value = validateEmail(email.value)
		inviteCodeError.value = validateInviteCode(inviteCode.value)

		when {
			emailError.value != null ||
					inviteCodeError.value != null -> return
			else -> subscription =
					inviteRepository.validateInviteCode(ValidateInviteRequest(inviteCode.value!!, email.value!!))
							.subscribeOn(Schedulers.io())
							.observeOn(AndroidSchedulers.mainThread())
							.doOnSubscribe { startLoadingEvent() }
							.doOnTerminate { stopLoadingEvent() }
							.subscribe(
									{ incrementStep() },
									{ result -> processApiError(result) }
							)
		}
	}

	private fun validateStep2() {
		firstNameError.value = validateName(firstName.value)
		lastNameError.value = validateName(lastName.value)
		passwordError.value = validatePassword(password.value)
		passwordConfirmationError.value = validatePasswordConfirmation(password.value, passwordConfirmation.value)

		when {
			firstNameError.value != null ||
					lastNameError.value != null ||
					passwordError.value != null ||
					passwordConfirmationError.value != null -> return
			else -> subscription = userRepository.registerAthlete(
					RegisterAthleteRequest(email.value!!,
							inviteCode.value!!,
							firstName.value!!,
							lastName.value!!,
							password.value!!,
							passwordConfirmation.value!!)
			)
					.subscribeOn(Schedulers.io())
					.observeOn(AndroidSchedulers.mainThread())
					.doOnSubscribe { startLoadingEvent() }
					.doOnTerminate { stopLoadingEvent() }
					.subscribe(
							{ incrementStep() },
							{ result -> processApiError(result) }
					)
		}
	}

	private fun processApiError(errorResponse: Throwable) {
		if (errorResponse is HttpException) {
			val apiErrors = parseApiError(errorResponse).errors

			val tmpInactiveError = getErrorMessagesForKey("inactive", apiErrors)
			val tmpReferralCodeError = getErrorMessagesForKey("referral_code", apiErrors)

			inviteCodeError.value = when {
				tmpInactiveError != null -> tmpInactiveError
				tmpReferralCodeError != null -> tmpReferralCodeError
				else -> null
			}
			emailError.value = getErrorMessagesForKey("email", apiErrors)
			passwordError.value = getErrorMessagesForKey("password", apiErrors)
			passwordConfirmationError.value = getErrorMessagesForKey("password", apiErrors)

		}
	}

	private fun decrementStep() {
		val tmpCurStep = curStep.value
		curStep.value = tmpCurStep?.minus(1)
	}

	private fun incrementStep() {
		val tmpCurStep = curStep.value
		curStep.value = tmpCurStep?.plus(1)
	}

	override fun onCleared() {
		super.onCleared()
		subscription?.dispose()
	}
}