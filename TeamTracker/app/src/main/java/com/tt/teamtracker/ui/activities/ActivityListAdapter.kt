package com.tt.teamtracker.ui.activities

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.tt.teamtracker.R
import com.tt.teamtracker.databinding.ActivityItemBinding
import com.tt.teamtracker.model.Activity
import com.tt.teamtracker.model.User

class ActivityListAdapter : RecyclerView.Adapter<ActivityListAdapter.ViewHolder>() {

	private lateinit var activityList: List<Activity>
	private lateinit var user: User

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActivityListAdapter.ViewHolder {
		val binding: ActivityItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.activity_item, parent, false)
		return ViewHolder(binding)
	}

	override fun getItemCount(): Int {
		return if (::activityList.isInitialized) activityList.size else 0
	}

	override fun onBindViewHolder(holder: ActivityListAdapter.ViewHolder, position: Int) {
		holder.bind(activityList[position], user)
	}

	fun updateActivityList(activityList: List<Activity>, user: User) {
		this.activityList = activityList
		this.user = user
		notifyDataSetChanged()
	}

	class ViewHolder(private val binding: ActivityItemBinding) : RecyclerView.ViewHolder(binding.root) {
		private val viewModel = ActivityViewModel()

		fun bind(activity: Activity, user: User) {
			viewModel.bind(activity, user)
			binding.viewModel = viewModel
		}
	}

}