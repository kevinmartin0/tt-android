package com.tt.teamtracker.base

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tt.teamtracker.R
import com.tt.teamtracker.databinding.FragmentRegisterAthleteStep1Binding
import com.tt.teamtracker.ui.register.athlete.RegisterAthleteViewModel

abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel> : Fragment() {

    protected lateinit var binding: T
    protected lateinit var viewModel: V

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewModel = ViewModelProviders.of(activity!!).get(getVMClass())
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        initViewModelBinding()
        return binding.root
    }

    @LayoutRes
    abstract fun getLayoutId(): Int

    abstract fun initViewModelBinding()

    abstract fun getVMClass(): Class<V>
}