package com.tt.teamtracker.ui.activities

import android.arch.lifecycle.MutableLiveData
import com.tt.teamtracker.base.BaseViewModel
import com.tt.teamtracker.model.Activity
import com.tt.teamtracker.model.User

class ActivityViewModel : BaseViewModel() {

	val activity = MutableLiveData<Activity>()
	val user = MutableLiveData<User>()

	fun bind(activity: Activity, user: User) {
		this.activity.value = activity
		this.user.value = user
	}
}