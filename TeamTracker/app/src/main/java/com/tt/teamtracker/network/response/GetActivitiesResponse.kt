package com.tt.teamtracker.network.response

import com.google.gson.annotations.SerializedName
import com.tt.teamtracker.model.Activity

data class GetActivitiesResponse(
		@SerializedName("activities")
		var activities : List<Activity>
)