package com.tt.teamtracker.ui.register.select

import com.tt.teamtracker.base.BaseViewModel
import com.tt.teamtracker.utils.SingleLiveEvent
import com.tt.teamtracker.utils.RegistrationTypes


class SelectRegistrationTypeViewModel : BaseViewModel() {

    val registrationType = SingleLiveEvent<RegistrationTypes>()

    fun onRegistrationTypeSelected(registrationType: RegistrationTypes) {
        this.registrationType.value = registrationType
    }

}

