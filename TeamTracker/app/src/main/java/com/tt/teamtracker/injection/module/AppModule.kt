package com.tt.teamtracker.injection.module

import dagger.Module
import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import javax.inject.Singleton
import dagger.Provides
import com.tt.teamtracker.utils.SharedPreferencesManager


@Module
class AppModule(val app: Application) {

	@Provides
	@Singleton
	fun providesApplication(): Application {
		return app
	}

	@Provides
	@Singleton
	fun providesSharedPreferences(application: Application): SharedPreferences {
		return PreferenceManager.getDefaultSharedPreferences(application)
	}

	@Singleton
	@Provides
	fun provideSharedPreferencesManager(sharedPreferences: SharedPreferences): SharedPreferencesManager {
		return SharedPreferencesManager(sharedPreferences)
	}
}