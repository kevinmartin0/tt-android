package com.tt.teamtracker.injection.component

import com.tt.teamtracker.repository.ActivityRespository
import com.tt.teamtracker.repository.InviteRepository
import com.tt.teamtracker.repository.UserRepository
import com.tt.teamtracker.injection.module.AppModule
import com.tt.teamtracker.injection.module.NetworkModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class])
interface RepositoryComponent {

	fun inject(userRepository: UserRepository)
	fun inject(inviteRepository: InviteRepository)
	fun inject(activityRespository: ActivityRespository)

	@Component.Builder
	interface Builder {
		fun build(): RepositoryComponent

		fun networkModule(networkModule: NetworkModule): Builder
		fun appModule(appModule: AppModule): Builder
	}
}