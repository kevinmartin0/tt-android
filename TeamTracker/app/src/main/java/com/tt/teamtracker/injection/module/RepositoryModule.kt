package com.tt.teamtracker.injection.module

import com.tt.teamtracker.repository.ActivityRespository
import com.tt.teamtracker.repository.InviteRepository
import com.tt.teamtracker.repository.UserRepository
import dagger.Module
import dagger.Provides

@Module
object RepositoryModule {
	@Provides
	fun providesUserRepository(): UserRepository {
		return UserRepository()
	}

	@Provides
	fun providesInviteRepository(): InviteRepository {
		return InviteRepository()
	}

	@Provides
	fun providesActivityRepository(): ActivityRespository {
		return ActivityRespository()
	}
}