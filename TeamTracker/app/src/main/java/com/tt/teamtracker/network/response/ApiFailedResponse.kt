package com.tt.teamtracker.network.response

import com.google.gson.annotations.SerializedName
import com.squareup.moshi.Json

data class ApiFailedResponse(
        @SerializedName(value = "error", alternate = ["errors"])
        val errors: MutableMap<String, MutableList<String>>
)
