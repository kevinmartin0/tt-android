package com.tt.teamtracker.network


import com.tt.teamtracker.network.request.LoginRequest
import com.tt.teamtracker.network.request.RegisterAthleteRequest
import com.tt.teamtracker.network.request.ValidateInviteRequest
import com.tt.teamtracker.network.response.*
import io.reactivex.Observable
import retrofit2.http.*


interface TeamTrackerApi {

	@Headers("Content-Type: application/json")
	@POST("api/invite/validate/")
	fun validateInviteCode(
			@Body body: ValidateInviteRequest
	): Observable<ApiSuccessResponse<ValidateInviteCodeResponse>>

	@Headers("Content-Type: application/json")
	@POST("api/register/by-invite/")
	fun registerAthlete(
			@Body body: RegisterAthleteRequest
	): Observable<ApiSuccessResponse<RegisterAthleteResponse>>

	@Headers("Content-Type: application/json")
	@POST("api/login/")
	fun login(
			@Body body: LoginRequest
	): Observable<ApiSuccessResponse<LoginResponse>>

	@Headers("Content-Type: application/json")
	@GET("api/activity/user")
	fun getActivities(
			@Header("Authorization") token: String
	): Observable<ApiSuccessResponse<GetActivitiesResponse>>

}