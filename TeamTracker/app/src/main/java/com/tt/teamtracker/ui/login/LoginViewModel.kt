package com.tt.teamtracker.ui.login

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.tt.teamtracker.base.BaseViewModel
import com.tt.teamtracker.repository.UserRepository
import com.tt.teamtracker.network.request.LoginRequest
import com.tt.teamtracker.network.response.ApiSuccessResponse
import com.tt.teamtracker.network.response.LoginResponse
import com.tt.teamtracker.utils.*
import com.tt.teamtracker.utils.errorEvents.ErrorWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import javax.inject.Inject

class LoginViewModel : BaseViewModel() {

	@Inject
	lateinit var userRepository: UserRepository

	val email = MutableLiveData<String>()
	val password = MutableLiveData<String>()

	val emailError = MutableLiveData<ErrorWrapper?>()
	val passwordError = MutableLiveData<ErrorWrapper?>()

	val navigateDashboardEvent = SingleLiveEvent<Any>()
	val navigateRegisterEvent = SingleLiveEvent<Any>()

	private var subscription: Disposable? = null

	fun login() {
		if (!isLoading()) {
			if (validateLogin()) {
				loginApiCall()
			}
		}
	}

	fun startNavigateDashboardEvent() {
		this.navigateDashboardEvent.call()
	}

	fun startNavigateRegisterEvent() {
		this.navigateRegisterEvent.call()
	}

	private fun validateLogin(): Boolean {
		emailError.value = validateEmail(email.value)
		passwordError.value = validatePassword(password.value)

		return when {
			emailError.value != null ||
					passwordError.value != null -> false
			else -> true
		}
	}

	private fun loginApiCall() {
		subscription = userRepository.authenticate(
				LoginRequest(
						email.value!!,
						password.value!!
				)

		)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.doOnSubscribe { startLoadingEvent() }
				.doOnTerminate { stopLoadingEvent() }
				.subscribe(
						{ successResponse -> onLoginSuccess(successResponse) },
						{ errorResponse -> processApiError(errorResponse) }
				)
	}

	private fun onLoginSuccess(response: ApiSuccessResponse<LoginResponse>) {
		val user = response.data.user
		user.token = "Bearer ${response.data.token}"
		userRepository.cacheUser(user)

		startNavigateDashboardEvent()
	}

	private fun processApiError(errorResponse: Throwable) {
		Log.i("TAGZ", "Some Api Error Occured")
		if (errorResponse is HttpException) {
			val apiErrors = parseApiError(errorResponse).errors
			Log.i("TAGZ", "It's an httpexception: $apiErrors")
			val errorMessage = getErrorMessagesForKey("login", apiErrors)
			emailError.value = errorMessage
			passwordError.value = errorMessage
		}
		//TODO: make else to show a dialog containing OOPS AN ERROR HAS OCCURRED
	}

	override fun onCleared() {
		super.onCleared()
		subscription?.dispose()
	}

}