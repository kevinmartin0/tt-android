package com.tt.teamtracker.network.response

import com.tt.teamtracker.model.Invite
import com.tt.teamtracker.model.Team

data class ValidateInviteCodeResponse(
        val invite: Invite,
        val team: Team
)