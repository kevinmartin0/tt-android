package com.tt.teamtracker.network.response

import com.tt.teamtracker.model.User

data class RegisterAthleteResponse(
        val token: String,
        val user: User
)