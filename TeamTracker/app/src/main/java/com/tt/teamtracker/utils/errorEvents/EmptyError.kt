package com.tt.teamtracker.utils.errorEvents

import android.support.annotation.StringRes
import com.tt.teamtracker.R

enum class EmptyError (@StringRes private val resourceId: Int) : ResourceError {
    EMPTY(R.string.empty_error);

    override fun getErrorResource() = resourceId
}