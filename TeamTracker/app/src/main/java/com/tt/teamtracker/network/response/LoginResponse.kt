package com.tt.teamtracker.network.response

import com.google.gson.annotations.SerializedName
import com.tt.teamtracker.model.User

data class LoginResponse(
		@SerializedName("token")
		val token: String,

		@SerializedName("user")
		val user: User
)