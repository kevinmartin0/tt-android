package com.tt.teamtracker.ui.activities

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tt.teamtracker.R
import com.tt.teamtracker.base.BaseFragment
import com.tt.teamtracker.databinding.FragmentActivitiesBinding
import com.tt.teamtracker.databinding.FragmentRegisterAthleteStep1Binding

class ActivitiesFragment: BaseFragment<FragmentActivitiesBinding, ActivitiesViewModel>(){

	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
		observeLoading()
	}

	private fun observeLoading() {
		viewModel.loadingEvent.observe(this, Observer { visibility -> binding.progressBar.visibility = visibility!! })
		binding.activityList.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
//		binding.activityList.adapter = viewModel.activityListAdapter
	}

	override fun getLayoutId(): Int {
		return R.layout.fragment_activities
	}

	override fun initViewModelBinding() {
		binding.viewModel = viewModel
	}

	override fun getVMClass(): Class<ActivitiesViewModel> {
		return ActivitiesViewModel::class.java
	}


}