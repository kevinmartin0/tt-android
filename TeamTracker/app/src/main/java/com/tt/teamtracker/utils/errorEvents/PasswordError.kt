package com.tt.teamtracker.utils.errorEvents

import android.support.annotation.StringRes
import com.tt.teamtracker.R

enum class PasswordError(@StringRes private val resourceId: Int) : ResourceError {
    EMPTY(R.string.empty_error),
    LENGTH(R.string.password_length_error),
    CONFIRMATION_MISMATCH(R.string.password_confirmation_error);

    override fun getErrorResource() = resourceId
}