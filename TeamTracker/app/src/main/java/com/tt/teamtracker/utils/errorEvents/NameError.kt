package com.tt.teamtracker.utils.errorEvents

import android.support.annotation.StringRes
import com.tt.teamtracker.R

enum class NameError (@StringRes private val resourceId: Int) : ResourceError {
    EMPTY(R.string.empty_error),
    LENGTH(R.string.name_length_error);

    override fun getErrorResource() = resourceId
}