package com.tt.teamtracker.model

import com.google.gson.annotations.SerializedName

data class Activity(
		@SerializedName("id")
		var id: Int,

		@SerializedName("user_id")
		var userId: Int,

		@SerializedName("sport")
		var sport: String,

		@SerializedName("title")
		var title: String,

		@SerializedName("type")
		var type: String,

		@SerializedName("datetime")
		var dateTime: String,

		@SerializedName("description")
		var description: String,

		@SerializedName("distance")
		var distance: Int,

		@SerializedName("elevation")
		var elevation: Int,

		@SerializedName("tss")
		var tss: Int,

		@SerializedName("duration_hr")
		var durationHour: Int,

		@SerializedName("duration_min")
		var durationMin: Int,

		@SerializedName("duration_sec")
		var durationSec: Int

)