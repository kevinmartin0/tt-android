package com.tt.teamtracker.ui.activities

import android.util.Log
import com.tt.teamtracker.base.BaseViewModel
import com.tt.teamtracker.repository.ActivityRespository
import com.tt.teamtracker.repository.UserRepository
import com.tt.teamtracker.model.Activity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ActivitiesViewModel : BaseViewModel() {

	@Inject
	lateinit var activityRepository: ActivityRespository

	@Inject
	lateinit var userRepository: UserRepository

	val activityListAdapter: ActivityListAdapter = ActivityListAdapter()

	private var subscription: Disposable? = null

	init {
		getActivities()
	}

	private fun getActivities() {
		subscription = activityRepository.getUserActivities(
				userRepository.getLoggedInUser()!!.token
		)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.doOnSubscribe { startLoadingEvent() }
				.doOnTerminate { stopLoadingEvent() }
				.subscribe(
						{ successResponse -> Log.i("TAGZ", "success: ${successResponse.data.activities}"); onGetActivitiesSuccess(successResponse.data.activities) },
						{ errorResponse -> Log.i("TAGZ", "error: ${errorResponse.message}") }
				)
	}

	private fun onGetActivitiesSuccess(activities: List<Activity>) {
		activityListAdapter.updateActivityList(activities, userRepository.getLoggedInUser()!!)
	}

	override fun onCleared() {
		super.onCleared()
		subscription?.dispose()
	}

}