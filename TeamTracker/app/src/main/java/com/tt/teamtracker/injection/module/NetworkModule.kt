package com.tt.teamtracker.injection.module

import com.tt.teamtracker.network.TeamTrackerApi
import com.tt.teamtracker.utils.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


@Module
object NetworkModule {

	@Provides
	@Reusable
	@JvmStatic
	internal fun providesTeamTrackerApi(retrofit: Retrofit): TeamTrackerApi {
		return retrofit.create(TeamTrackerApi::class.java)
	}

	@Provides
	@Reusable
	@JvmStatic
	internal fun providesClient(): OkHttpClient {
		return OkHttpClient.Builder()
				.addInterceptor(HttpLoggingInterceptor()
						.setLevel(HttpLoggingInterceptor.Level.BODY))
				.build()
	}

	@Provides
	@Reusable
	@JvmStatic
	internal fun providesRetrofitInterface(okHttpClient: OkHttpClient): Retrofit {
		return Retrofit.Builder()
				.baseUrl(BASE_URL)
				.client(okHttpClient)
				.addConverterFactory(GsonConverterFactory.create())
				.addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
				.build()
	}

}