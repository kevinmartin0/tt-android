package com.tt.teamtracker.ui.register.athlete

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.View
import android.widget.Toast
import com.tt.teamtracker.R
import com.tt.teamtracker.base.BaseActivity
import com.tt.teamtracker.databinding.ActivityRegisterAthleteBinding
import com.tt.teamtracker.ui.login.LoginActivity
import com.tt.teamtracker.ui.register.coach.RegisterCoachActivity
import com.tt.teamtracker.utils.RegistrationTypes
import kotlinx.android.synthetic.main.activity_select_registration_type.*

class RegisterAthleteActivity : BaseActivity<ActivityRegisterAthleteBinding, RegisterAthleteViewModel>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeCurrentStep()
        observeNavigateLogin()
        observeLoading()
    }

    private fun observeCurrentStep() {
        viewModel.curStep.observe(this, Observer { curStep ->
            when (curStep) {
                1 -> showStep1()
                2 -> showStep2()
                3 -> showStep3()
            }
        })
    }

    private fun observeLoading() {
        viewModel.loadingEvent.observe(this, Observer { visibility -> binding.progressBar.visibility = visibility!! })
    }

    private fun showStep1() {
        addFragment(RegisterAthleteStep1Fragment(), false, "step1", getFragmentContainer())
    }

    private fun showStep2() {
        addFragment(RegisterAthleteStep2Fragment(), false, "step2", getFragmentContainer())
    }

    private fun showStep3() {
        addFragment(RegisterAthleteStep3Fragment(), false, "step3", getFragmentContainer())
        hideStepNavigation()
    }

    private fun hideStepNavigation() {
        binding.backBt.visibility = View.GONE
        binding.nextBt.visibility = View.GONE
    }

    private fun showStepNavigation() {
        binding.backBt.visibility = View.VISIBLE
        binding.nextBt.visibility = View.VISIBLE
    }

    private fun observeNavigateLogin() {
        viewModel.navigateLoginEvent.observe(this, Observer { startNewActivity(LoginActivity::class.java, false) })
    }

    private fun getFragmentContainer(): Int {
        return R.id.fragment_container
    }

    override fun getVMClass(): Class<RegisterAthleteViewModel> {
        return RegisterAthleteViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_register_athlete
    }

    override fun initViewModelBinding() {
        binding.viewModel = viewModel
    }

    override fun getActivityTitle(): String {
        return "Register Athlete"
    }
}