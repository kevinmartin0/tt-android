package com.tt.teamtracker.utils

import android.util.Patterns
import com.tt.teamtracker.utils.errorEvents.*

fun validateEmail(email: String?): ErrorWrapper? {
    return when {
        email.isNullOrBlank() -> ErrorWrapper(EmailError.EMPTY)
        !Patterns.EMAIL_ADDRESS.matcher(email).matches() -> ErrorWrapper(EmailError.INVALID_FORMAT)
        else -> {
            null
        }
    }
}

fun validateInviteCode(inviteCode: String?): ErrorWrapper? {
    return when {
        inviteCode.isNullOrBlank() -> ErrorWrapper(EmptyError.EMPTY)
        else -> {
            null
        }
    }
}

fun validateName(name: String?): ErrorWrapper? {
    return when {
        name.isNullOrBlank() -> ErrorWrapper(NameError.EMPTY)
        !lengthBetween(name!!, 3, 35) -> ErrorWrapper(NameError.LENGTH)
        else -> {
            null
        }
    }
}

fun validatePassword(password: String?): ErrorWrapper? {
    return when {
        password.isNullOrBlank() -> ErrorWrapper(PasswordError.EMPTY)
        !lengthBetween(password!!, 3, 12) -> ErrorWrapper(PasswordError.LENGTH)
        else -> {
            null
        }
    }
}

fun validatePasswordConfirmation(password: String?, passwordConfirmation: String?): ErrorWrapper? {
    return when {
        passwordConfirmation.isNullOrBlank() -> ErrorWrapper(PasswordError.EMPTY)
        !password.equals(passwordConfirmation) -> ErrorWrapper(PasswordError.CONFIRMATION_MISMATCH)
        else -> {
            null
        }
    }
}

fun lengthBetween(value: String, min: Int, max: Int): Boolean {
    return value.length in min..max
}
