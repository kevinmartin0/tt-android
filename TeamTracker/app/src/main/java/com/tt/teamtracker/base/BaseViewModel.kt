package com.tt.teamtracker.base

import android.arch.lifecycle.ViewModel
import android.view.View
import com.tt.teamtracker.injection.component.DaggerViewModelComponent
import com.tt.teamtracker.injection.component.ViewModelComponent
import com.tt.teamtracker.injection.module.NetworkModule
import com.tt.teamtracker.injection.module.RepositoryModule
import com.tt.teamtracker.ui.activities.ActivitiesViewModel
import com.tt.teamtracker.ui.dashboard.DashboardViewModel
import com.tt.teamtracker.ui.login.LoginViewModel
import com.tt.teamtracker.ui.register.athlete.RegisterAthleteViewModel
import com.tt.teamtracker.ui.splash.SplashViewModel
import com.tt.teamtracker.utils.SingleLiveEvent

abstract class BaseViewModel : ViewModel() {

	val loadingEvent = SingleLiveEvent<Int>()

	private val component: ViewModelComponent = DaggerViewModelComponent
			.builder()
			.repositoryModule(RepositoryModule)
			.build()

	init {
		inject()
		loadingEvent.value = View.GONE
	}

	private fun inject() {
		when (this) {
			is RegisterAthleteViewModel -> component.inject(this)
			is LoginViewModel -> component.inject(this)
			is DashboardViewModel -> component.inject(this)
			is SplashViewModel -> component.inject(this)
			is ActivitiesViewModel -> component.inject(this)
		}
	}

	protected fun startLoadingEvent() {
		loadingEvent.value = View.VISIBLE
	}

	protected fun stopLoadingEvent() {
		loadingEvent.value = View.GONE
	}

	protected fun isLoading(): Boolean {
		return when (loadingEvent.value) {
			null, View.GONE -> false
			View.VISIBLE -> true
			else -> false
		}
	}
}