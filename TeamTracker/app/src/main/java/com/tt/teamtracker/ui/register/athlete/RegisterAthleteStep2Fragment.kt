package com.tt.teamtracker.ui.register.athlete

import com.tt.teamtracker.R
import com.tt.teamtracker.base.BaseFragment
import com.tt.teamtracker.databinding.FragmentRegisterAthleteStep2Binding

class RegisterAthleteStep2Fragment : BaseFragment<FragmentRegisterAthleteStep2Binding, RegisterAthleteViewModel>() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_register_athlete_step_2
    }

    override fun initViewModelBinding() {
        binding.viewModel = viewModel
    }

    override fun getVMClass(): Class<RegisterAthleteViewModel> {
        return RegisterAthleteViewModel::class.java
    }

}