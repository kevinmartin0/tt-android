package com.tt.teamtracker.ui.register.coach

import com.tt.teamtracker.R
import com.tt.teamtracker.base.BaseActivity
import com.tt.teamtracker.databinding.ActivityRegisterCoachBinding

class RegisterCoachActivity : BaseActivity<ActivityRegisterCoachBinding, RegisterCoachViewModel>() {

    override fun getLayoutId(): Int {
        return R.layout.activity_register_coach
    }

    override fun initViewModelBinding() {
        binding.viewModel = viewModel
    }

    override fun getVMClass(): Class<RegisterCoachViewModel> {
        return RegisterCoachViewModel::class.java
    }

    override fun getActivityTitle(): String {
        return "Register Coach"
    }

}