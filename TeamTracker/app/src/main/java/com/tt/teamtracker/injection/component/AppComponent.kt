package com.tt.teamtracker.injection.component

import com.tt.teamtracker.base.App
import com.tt.teamtracker.injection.module.AppModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
	fun inject(app: App)
}