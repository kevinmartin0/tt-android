package com.tt.teamtracker.ui.register.select

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.DataBindingUtil.setContentView
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import com.tt.teamtracker.R
import com.tt.teamtracker.base.BaseActivity
import com.tt.teamtracker.databinding.ActivitySelectRegistrationTypeBinding
import com.tt.teamtracker.ui.register.athlete.RegisterAthleteActivity
import com.tt.teamtracker.ui.register.coach.RegisterCoachActivity
import com.tt.teamtracker.utils.RegistrationTypes

class SelectRegistrationTypeActivity : BaseActivity<ActivitySelectRegistrationTypeBinding, SelectRegistrationTypeViewModel>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeRegistrationTypeSelection()
    }

    private fun observeRegistrationTypeSelection() {
        viewModel.registrationType.observe(this, Observer { registrationType ->
            when (registrationType) {
                RegistrationTypes.COACH -> startNewActivity(RegisterCoachActivity::class.java, false)
                RegistrationTypes.ATHLETE -> startNewActivity(RegisterAthleteActivity::class.java, false)
            }

        })
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_select_registration_type
    }

    override fun initViewModelBinding() {
        binding.viewModel = viewModel
    }

    override fun getVMClass(): Class<SelectRegistrationTypeViewModel> {
        return SelectRegistrationTypeViewModel::class.java
    }

    override fun getActivityTitle(): String {
        return "Select your role"
    }
}