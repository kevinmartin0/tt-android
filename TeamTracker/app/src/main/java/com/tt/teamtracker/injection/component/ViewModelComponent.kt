package com.tt.teamtracker.injection.component

import com.tt.teamtracker.injection.module.RepositoryModule
import com.tt.teamtracker.ui.activities.ActivitiesViewModel
import com.tt.teamtracker.ui.dashboard.DashboardViewModel
import com.tt.teamtracker.ui.login.LoginViewModel
import com.tt.teamtracker.ui.register.athlete.RegisterAthleteViewModel
import com.tt.teamtracker.ui.splash.SplashViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RepositoryModule::class])
interface ViewModelComponent {
    fun inject(registerAthleteViewModel: RegisterAthleteViewModel)
    fun inject(loginViewModel: LoginViewModel)
	fun inject(dashboardViewModel: DashboardViewModel)
	fun inject(splashViewModel: SplashViewModel)
	fun inject(activitiesViewModel: ActivitiesViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelComponent

        fun repositoryModule(repositoryModule: RepositoryModule): Builder
    }
}
