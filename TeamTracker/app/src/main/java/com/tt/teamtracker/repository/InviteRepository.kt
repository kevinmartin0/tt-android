package com.tt.teamtracker.repository

import com.tt.teamtracker.base.BaseRepository
import com.tt.teamtracker.network.request.ValidateInviteRequest
import com.tt.teamtracker.network.response.ApiSuccessResponse
import com.tt.teamtracker.network.response.ValidateInviteCodeResponse
import io.reactivex.Observable

class InviteRepository : BaseRepository() {
	fun validateInviteCode(validateInviteRequest: ValidateInviteRequest): Observable<ApiSuccessResponse<ValidateInviteCodeResponse>> {
		return teamTrackerApi.validateInviteCode(validateInviteRequest)
	}
}