package com.tt.teamtracker.ui.splash

import android.os.Handler
import com.tt.teamtracker.base.BaseViewModel
import com.tt.teamtracker.repository.UserRepository
import com.tt.teamtracker.utils.SingleLiveEvent
import javax.inject.Inject

class SplashViewModel : BaseViewModel() {

	@Inject
	lateinit var userRepository: UserRepository

	val navigateDashboardEvent = SingleLiveEvent<Any>()
	val navigateLoginEvent = SingleLiveEvent<Any>()

	init {
		Handler().postDelayed({
			if (userRepository.isLoggedIn()) startNavigateDashboardEvent()
			else startNavigateLoginEvent()
		}, 3000)
	}

	private fun startNavigateDashboardEvent() {
		navigateDashboardEvent.call()
	}

	private fun startNavigateLoginEvent() {
		navigateLoginEvent.call()
	}

}