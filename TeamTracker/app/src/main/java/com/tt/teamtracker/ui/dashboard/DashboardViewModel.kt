package com.tt.teamtracker.ui.dashboard

import android.arch.lifecycle.MutableLiveData
import com.tt.teamtracker.base.BaseViewModel
import com.tt.teamtracker.repository.UserRepository
import com.tt.teamtracker.utils.SingleLiveEvent
import javax.inject.Inject

class DashboardViewModel : BaseViewModel() {

	@Inject
	lateinit var userRepository: UserRepository

	val username = MutableLiveData<String>()
	val logoutEvent = SingleLiveEvent<Any>()

	init {
		val loggedInUser = userRepository.getLoggedInUser()
		username.value = "${loggedInUser?.firstName} ${loggedInUser?.lastName}"
	}

	fun logout() {
		userRepository.logout()
		logoutEvent.call()
	}
}