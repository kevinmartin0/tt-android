package com.tt.teamtracker.repository

import com.tt.teamtracker.base.BaseRepository
import com.tt.teamtracker.model.User
import com.tt.teamtracker.network.request.LoginRequest
import com.tt.teamtracker.network.request.RegisterAthleteRequest
import com.tt.teamtracker.network.response.ApiSuccessResponse
import com.tt.teamtracker.network.response.LoginResponse
import com.tt.teamtracker.network.response.RegisterAthleteResponse
import com.tt.teamtracker.utils.SharedPreferencesManager
import io.reactivex.Observable
import javax.inject.Inject

class UserRepository : BaseRepository() {
	@Inject
	lateinit var sharedPreferencesManager: SharedPreferencesManager

	fun authenticate(loginRequest: LoginRequest): Observable<ApiSuccessResponse<LoginResponse>> {
		return teamTrackerApi.login(loginRequest)
	}

	fun registerAthlete(registerAthleteRequest: RegisterAthleteRequest): Observable<ApiSuccessResponse<RegisterAthleteResponse>> {
		return teamTrackerApi.registerAthlete(registerAthleteRequest)
	}

	fun cacheUser(user: User?) {
		sharedPreferencesManager.setUser(user)
	}

	fun getLoggedInUser(): User? {
		return sharedPreferencesManager.getUser()
	}

	fun isLoggedIn(): Boolean {
		return sharedPreferencesManager.isLoggedIn()
	}

	fun logout() {
		sharedPreferencesManager.removeUser()
	}
}