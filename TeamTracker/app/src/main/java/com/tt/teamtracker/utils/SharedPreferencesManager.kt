package com.tt.teamtracker.utils

import android.content.SharedPreferences
import com.google.gson.Gson
import com.tt.teamtracker.model.User

class SharedPreferencesManager(private val sharedPreferences: SharedPreferences) {

	companion object {
		const val USER_KEY = "user"
	}

	fun setUser(user: User?) {
		sharedPreferences.edit().putString(USER_KEY, Gson().toJson(user)).apply()
	}

	fun getUser(): User? {
		return Gson().fromJson(sharedPreferences.getString(USER_KEY, null), User::class.java)
	}

	fun isLoggedIn(): Boolean {
		return getUser() != null
	}

	fun removeUser() {
		sharedPreferences.edit().remove(USER_KEY).apply()
	}

	fun clearAll() {
		sharedPreferences.edit().clear().apply()
	}
}