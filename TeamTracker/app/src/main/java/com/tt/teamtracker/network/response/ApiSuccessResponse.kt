package com.tt.teamtracker.network.response

import com.google.gson.annotations.SerializedName

data class ApiSuccessResponse<T>(
		@SerializedName("success")
		val data: T
)