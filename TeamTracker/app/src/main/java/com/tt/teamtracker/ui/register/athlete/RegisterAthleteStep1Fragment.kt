package com.tt.teamtracker.ui.register.athlete

import com.tt.teamtracker.R
import com.tt.teamtracker.base.BaseFragment
import com.tt.teamtracker.databinding.FragmentRegisterAthleteStep1Binding

class RegisterAthleteStep1Fragment : BaseFragment<FragmentRegisterAthleteStep1Binding, RegisterAthleteViewModel>() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_register_athlete_step_1
    }

    override fun initViewModelBinding() {
        binding.viewModel = viewModel
    }

    override fun getVMClass(): Class<RegisterAthleteViewModel> {
        return RegisterAthleteViewModel::class.java
    }
}