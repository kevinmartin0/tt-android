package com.tt.teamtracker.model

import com.google.gson.annotations.SerializedName

data class User (
		@SerializedName("token")
		var token: String,

		@SerializedName("id")
		var id : Int,

		@SerializedName("first_name")
		var firstName : String,

		@SerializedName("last_name")
		var lastName : String,

		@SerializedName("email")
		var email : String,

		@SerializedName("role_type")
		var roleType : String,

		@SerializedName("avatar")
		var avatarPath : String
)

