package com.tt.teamtracker.utils.errorEvents

import android.content.Context

class ErrorWrapper(var message: Any) {
    fun getStringMessage(context: Context): String {
        return when (message) {
            is ResourceError -> context.getString((message as ResourceError).getErrorResource())
            else -> message.toString()
        }
    }
}