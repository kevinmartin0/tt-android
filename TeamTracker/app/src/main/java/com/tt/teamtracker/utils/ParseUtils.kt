package com.tt.teamtracker.utils

import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.tt.teamtracker.network.response.ApiFailedResponse
import com.tt.teamtracker.utils.errorEvents.ErrorWrapper
import retrofit2.HttpException

fun getErrorMessagesForKey(key: String, errors: Map<String, MutableList<String>>): ErrorWrapper? {
    if (errors.containsKey(key)) {
        return ErrorWrapper(errors[key]!!.joinToString(separator = " "))
    }
    return null
}

fun parseApiError(response: HttpException): ApiFailedResponse {
    val errorBody = response.response().errorBody()!!
    return if (errorBody.contentType()!!.toString() == "application/json") {
        val errors = errorBody.string()
        try {
            Gson().fromJson(errors, ApiFailedResponse::class.java)
        } catch (e: JsonSyntaxException) {
            Log.d("ParseApiError", "Could not parse Api Error with body: $errors")
            ApiFailedResponse(mutableMapOf("parse_exception" to mutableListOf("An unknown error has occurred")))
        }
    } else {
        ApiFailedResponse(mutableMapOf())
    }
}