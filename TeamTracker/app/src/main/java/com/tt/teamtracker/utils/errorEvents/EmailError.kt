package com.tt.teamtracker.utils.errorEvents

import android.support.annotation.StringRes
import com.tt.teamtracker.R

enum class EmailError(@StringRes private val resourceId: Int) : ResourceError {
    EMPTY(R.string.empty_error),
    INVALID_FORMAT(R.string.email_format_error);

    override fun getErrorResource() = resourceId
}
