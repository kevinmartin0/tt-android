package com.tt.teamtracker.network.request

import com.google.gson.annotations.SerializedName
import retrofit2.http.Field

data class RegisterAthleteRequest(
        @SerializedName("email")
        val email: String,

        @SerializedName("referral_code")
        val inviteCode: String,

        @SerializedName("first_name")
        val firstName: String,

        @SerializedName("last_name")
        val lastName: String,

        @SerializedName("password")
        val password: String,

        @SerializedName("confirm_password")
        val passwordConfirm: String
)