package com.tt.teamtracker.network.request

import com.google.gson.annotations.SerializedName

data class ValidateInviteRequest(
        @SerializedName("referral_code")
        val inviteCode: String,

        @SerializedName("email")
        val email: String
)