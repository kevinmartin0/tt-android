package com.tt.teamtracker.ui.splash

import android.arch.lifecycle.Observer
import android.os.Bundle
import com.bumptech.glide.Glide
import com.tt.teamtracker.R
import com.tt.teamtracker.base.BaseActivity
import com.tt.teamtracker.databinding.ActivitySplashBinding
import com.tt.teamtracker.ui.dashboard.DashboardActivity
import com.tt.teamtracker.ui.login.LoginActivity

class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		initLoadingGif()
		observeNavigateDashboard()
		observeNavigateLogin()
	}

	private fun initLoadingGif() {
		Glide.with(this)
				.load(R.drawable.splash_loading)
				.into(binding.splashGifTv)
	}

	private fun observeNavigateLogin() {
		viewModel.navigateLoginEvent.observe(this, Observer {
			finish()
			startNewActivity(LoginActivity::class.java, true)
		})
	}

	private fun observeNavigateDashboard() {
		viewModel.navigateDashboardEvent.observe(this, Observer {
			finish()
			startNewActivity(DashboardActivity::class.java, true)
		})
	}

	override fun getLayoutId(): Int {
		return R.layout.activity_splash
	}

	override fun initViewModelBinding() {
		binding.viewModel = viewModel
	}

	override fun getVMClass(): Class<SplashViewModel> {
		return SplashViewModel::class.java
	}

	override fun getActivityTitle(): String {
		return "Splash!"
	}

}