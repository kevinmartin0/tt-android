package com.tt.teamtracker.utils

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.databinding.BindingAdapter
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.tt.teamtracker.R
import android.support.design.widget.TextInputLayout
import com.tt.teamtracker.utils.errorEvents.ErrorWrapper


@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
	view.adapter = adapter
}

@BindingAdapter("mutableVisibility")
fun setMutableVisibility(view: View, visibility: MutableLiveData<Int>?) {
	val parentActivity: AppCompatActivity? = view.getParentActivity()
	if (parentActivity != null && visibility != null) {
		visibility.observe(parentActivity, Observer { value -> view.visibility = value ?: View.VISIBLE })
	}
}

@BindingAdapter("mutableText")
fun setMutableText(view: TextView, text: MutableLiveData<String>?) {
	val parentActivity: AppCompatActivity? = view.getParentActivity()
	if (parentActivity != null && text != null) {
		text.observe(parentActivity, Observer { value -> view.text = value ?: "" })
	}
}

@BindingAdapter("android:onClick", "android:clickable")
fun setOnClick(view: View, clickListener: View.OnClickListener,
			   clickable: Boolean) {
	view.setOnClickListener(clickListener)
	view.isClickable = clickable
}

@BindingAdapter(value = ["mutableStepsBackgroundColor", "step"])
fun setBackgroundColor(view: TextView, curStep: MutableLiveData<Int>?, step: Int) {
	val parentActivity: AppCompatActivity? = view.getParentActivity()
	if (parentActivity != null && curStep != null) {
		curStep.observe(parentActivity, Observer { value ->
			view.setBackgroundColor(parentActivity.resources.getColor(
					if (value!! >= step) R.color.blue else R.color.white,
					null
			))
		})
	}
}

@BindingAdapter("errorText")
fun setErrorMessage(view: TextInputLayout, error: MutableLiveData<ErrorWrapper>?) {
	val parentActivity: AppCompatActivity? = view.getParentActivity()
	if (parentActivity != null && error != null) {
		error.observe(parentActivity, Observer { value ->
			if (value != null)
				view.error = value.getStringMessage(parentActivity)
			else view.error = ""
		})
	}
}

