package com.tt.teamtracker.base

import com.tt.teamtracker.repository.ActivityRespository
import com.tt.teamtracker.repository.InviteRepository
import com.tt.teamtracker.repository.UserRepository
import com.tt.teamtracker.injection.component.DaggerRepositoryComponent
import com.tt.teamtracker.injection.component.RepositoryComponent
import com.tt.teamtracker.injection.module.AppModule
import com.tt.teamtracker.injection.module.NetworkModule
import com.tt.teamtracker.network.TeamTrackerApi
import javax.inject.Inject


abstract class BaseRepository {

	@Inject
	lateinit var teamTrackerApi: TeamTrackerApi

	private val component: RepositoryComponent = DaggerRepositoryComponent
			.builder()
			.networkModule(NetworkModule)
			.appModule(AppModule(App.instance))
			.build()

	init {
		inject()
	}

	private fun inject() {
		when (this) {
			is UserRepository -> component.inject(this)
			is InviteRepository -> component.inject(this)
			is ActivityRespository -> component.inject(this)
		}
	}
}