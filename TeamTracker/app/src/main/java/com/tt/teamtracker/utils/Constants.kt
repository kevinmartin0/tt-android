package com.tt.teamtracker.utils

enum class RegistrationTypes {
    COACH, ATHLETE
}

const val BASE_URL: String = "http://10.0.2.2:8000/"
